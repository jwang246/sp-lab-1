#include<stdio.h>
#include"file_util.h"
FILE *fp;//global file pointer to fetch file
/*
close file using I/O stream	
*/
void close_file(){
	fclose(fp);
}
/*
open file using file name passed by command line
char *c indicates the operation including(read only, write only etc.)
return 0 if success else return -1	
*/
int open_file(char* fn, char* c){
	int flag=0;
	if((fp=fopen(fn,c))==NULL){
		flag=-1;
		printf("the file cannot be opened\n");
	}
	return flag;
}
/*
read integer from file and store into p 
return 0 if success else -1
*/
int read_int(int* p){
	int flag=-1;
	if(fscanf(fp,"%d", p)==1){
		flag=0;
	}	
	return flag;
}
/*
read string from file and store into c
return 0 if success else -1
*/
int read_string(char* c){
	int flag=-1;
	if(fscanf(fp,"%s", c)==1){
		flag=0;
	}
	return flag;	
}
/*
read DB file through the file name passed by command line 
and store the value into structue array emp[i]
return 0 if success else -1
*/
int read_file(char* c){
	int flag;
	int i=0;
	if((flag=open_file(c,"r"))==0){
		while(read_int(&emp[i].id)==0){
			read_string(emp[i].fname);
			read_string(emp[i].lname);
			read_int(&emp[i].salary);
			i++;
		}
			count=i;	
	}
	else{
		flag=-1;
	}
	close_file();
	return flag;
}
/*
write data from structure array emp[i] into database file
return 0 if success else -1
*/
int write_file(char* c){
	int flag=0;
	if((flag=open_file(c,"a"))==0){		
		fprintf(fp,"\n%d %s %s %d",emp[count].id,emp[count].fname,emp[count].lname,emp[count].salary);
	
	}
	else{
		flag=-1;
	}
	close_file();
	return flag;
}

#include<stdio.h>
#include<string.h>
#include"file_util.h"
#include"view.h"
/*
print out the data fetched from DB 
in aphabatically ascending order
*/
void PrintDB(char* c){
	void SortID();
	read_file(c);	
	SortID();
	printf("\nNAME\t\t\t\tSALARY\t\tID\n");
	printf("------------------------------------------------------------\n");
	for(int i=0;i<count;i++){
	printf("%-10s\t%-16s%-15d\t%d\n",emp[i].fname,emp[i].lname,emp[i].salary,emp[i].id);
	}
	printf("------------------------------------------------------------\n");
	printf(" Number of Employees (%d)\n\n", count);
	Mainmenu(c);
}	
/*  
sort structure array by ID and then write back data
into databse; return 1 if success, else 0
*/
void SortID(){
	for(int i=0;i<count;i++){
		for(int j=0;j<count-i-1;j++){//bubble sort
			if(emp[j].id>emp[j+1].id){
				EMP tmp=emp[j+1];
				emp[j+1]=emp[j];
				emp[j]=tmp;
			}
		}
	}

}
/*
read DB file and find the id matching employee by binary search
and print the information; return 0 when it success else -1
*/
int FindID(char *c){
	read_file(c);	
	SortID();
	int input;
	printf("Enter a 6 digit employee id: ");//prompt user to enter the ID 
	scanf("%d",&input);
	int head,tail,mid;//binary search
	int flag=0;	
	head=0;
	tail=count-1;	
	while(head<=tail){
		mid=(head+tail)/2;
		if(input<emp[mid].id){
			tail=mid-1;
		}
		else if(input>emp[mid].id){
			head=mid+1;
		}
		else {
			flag=1;
			break;
		}
	}
	if(flag==0){
		printf("Employee with id %d is not found in DB\n\n",input);	
	}
	if(flag==1){
		printf("\nNAME\t\t\t\tSALARY\t\tID\n");
		printf("------------------------------------------------------------\n");
		printf("%-10s\t%-16s%-15d\t%d\n",emp[mid].fname,emp[mid].lname,emp[mid].salary,emp[mid].id);
		printf("------------------------------------------------------------\n");
	}
	Mainmenu(c);
	return flag;
}
/*
read DB file and find the fname matching employee by binary search
and print the information; return 0 when it success else -1
*/
int FindLname(char* c){
	read_file(c);	
	char input[15];
	int flag=0;
	printf("Enter Employee's last name (no extra space): ");
	scanf("%s",input);//prompt user to enter lastname
	for(int i=0;i<count;i++){//traverse the stucts array and find the matching data
		if(strcasecmp(input,emp[i].lname)==0){
			printf("\nNAME\t\t\t\tSALARY\t\tID\n");
			printf("------------------------------------------------------------\n");
			printf("%-10s\t%-16s%-15d\t%d\n",emp[i].fname,emp[i].lname,emp[i].salary,emp[i].id);
			printf("------------------------------------------------------------\n");
			flag=1;
			break;
		}
	}
	if (flag==0)printf("Employee is not found\n");
	Mainmenu(c);
	return flag;
}
/*
add a new employee into DB inputed by user
*/
void AddEmp(char* c){
	char fname[MAXNAME];
	char lname[MAXNAME];
	int id;
	int salary;
	printf("Enter the first name of the employee: ");
	scanf("%s",fname);
	printf("\nEnter the last name of the employee: ");	
	scanf("%s",lname);
	printf("\nEnter a 6-digit ID: ");
	scanf("%d",&id);
	printf("\nEnter employee's salary (30000 to 150000): ");
	scanf("%d",&salary);
	printf("\nDo you want to add the following employee into the DB?\n");
	printf("\t%s %s, salary: %d, id: %d\n",fname,lname,salary,id);
	printf("Enter 1 for yes, 0 for no: ");
	int input;
	scanf("%d",&input);
	if(input==1){
		strcpy(emp[count].fname, fname);//write data into stucts and then write into DB file
		strcpy(emp[count].lname, lname);
		emp[count].id=id;
		emp[count].salary=salary;
		write_file(c);	
	}
	Mainmenu(c);
}

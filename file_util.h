#ifndef _FILEUTIL_H_
#define _FILEUTIL_H_
#include<stdio.h>
int open_file(char* filename,char* c);//open file through filename entered by user and instruction specified by c return 0 if successful, else -1
int read_string(char *p);//read string and store into p;if success return 0, else return -1
int read_int(int *p);//read integer and store into p; if succes return 0, else -1
void close_file();//close file
int read_file(char* filename);//read file through filename passed by user, return 0 if success, else -1
int write_file(char* filename);//write into file through filename passed by user, return 0 if success, else -1
#define MAXNAME 10	//the maximum character of name
#define MAXID 6	//the maximum num of digits of ID 	
#define MAXEMP 1024	//the maximum number of employee in database
int count;// the number of exsiting employees in the database
typedef struct{
	int id;
	char fname[MAXNAME];
	char lname[MAXNAME];
	int salary;
}EMP;
EMP emp[MAXEMP];
#endif


#include <stdio.h>
#include"service.h"
/*
print mainmenu of the system and prompt user enter 
check the input number is legal or not and enter into 
corresponding interface
param: cp is the filename got from command line
*/
void Mainmenu(char* cp){
 	printf("%s\n","Employee DB Menu");
	printf("%s\n","--------------------------------");
	printf("%s\n","  (1) Print the Database");
	printf("%s\n","  (2) Lookup by ID");
	printf("%s\n","  (3) Lookup by Last Name");
	printf("%s\n","  (4) Add an Employee");
	printf("%s\n","  (5) Quit");
	printf("%s\n","--------------------------------");
	printf("%s","Enter your choice: ");
	int input;
	int ret = scanf("%d",&input);
	while(1){
		while(ret !=1 ){  //handling illegal input
			printf("Please enter legal number!!\n");
			printf("Enter your choice again: ");
			while (getchar() != '\n');
			ret = scanf("%d",&input);
		}
		if(input<1 | input>5){
			printf("\nHey, %d is not between 1 and 5, try again...\n",input);
			printf("Enter your choice again: ");
			scanf("%d",&input);
		}
		else{
			break;
		}
	}
	switch(input){
		case 1:
			PrintDB(cp);
			break;
		case 2:
			FindID(cp);
			break;
		case 3:
			FindLname(cp);	
			break;
		case 4:
			AddEmp(cp);
			break;
		case 5:
			printf("Goodbye!!!");
			break;
	}
}
